import argparse
import os
import time
import pickle
import pdb
import cv2
import numpy as np

import torch
from torch.utils.model_zoo import load_url
from torchvision import transforms

from cirtorch.networks.imageretrievalnet import init_network, extract_vectors
from cirtorch.datasets.datahelpers import cid2filename
from cirtorch.datasets.testdataset import configdataset
from cirtorch.utils.download import download_train, download_test
from cirtorch.utils.whiten import whitenlearn, whitenapply
from cirtorch.utils.evaluate import compute_map_and_print
from cirtorch.utils.general import get_data_root, htime

PRETRAINED = {
    'retrievalSfM120k-vgg16-gem'        : 'http://cmp.felk.cvut.cz/cnnimageretrieval/data/networks/retrieval-SfM-120k/retrievalSfM120k-vgg16-gem-b4dcdc6.pth',
    'retrievalSfM120k-resnet101-gem'    : 'http://cmp.felk.cvut.cz/cnnimageretrieval/data/networks/retrieval-SfM-120k/retrievalSfM120k-resnet101-gem-b80fb85.pth',
    # new networks with whitening learned end-to-end
}


whitening_names = ['retrieval-SfM-30k', 'retrieval-SfM-120k']

parser = argparse.ArgumentParser(description='PyTorch CNN Image Retrieval Testing')

# network
group = parser.add_mutually_exclusive_group(required=False)
group.add_argument('--network-path', '-npath', metavar='NETWORK', default='retrievalSfM120k-resnet101-gem',
                    help="pretrained network or network path (destination where network is saved)")


# test options

parser.add_argument('--input-img', '-img', metavar='INPUT', default = 'data/test/oxford5k/jpg/all_souls_000202.jpg',
                    help="intput to retrieval")

parser.add_argument('--image-size', '-imsize', default=1024, type=int, metavar='N',
                    help="maximum size of longer image side used for testing (default: 1024)")

parser.add_argument('--multiscale', '-ms', metavar='MULTISCALE', default='[1, 1/2**(1/2), 1/2]', 
                    help="use multiscale vectors for testing, " + 
                    " examples: '[1]' | '[1, 1/2**(1/2), 1/2]' | '[1, 2**(1/2), 1/2**(1/2)]' (default: '[1]')")
parser.add_argument('--whitening', '-w', metavar='WHITENING', default='retrieval-SfM-120k', choices=whitening_names,
                    help="dataset used to learn whitening for testing: " + 
                        " | ".join(whitening_names) + 
                        " (default: None)")

# GPU ID
parser.add_argument('--gpu-id', '-g', default='1', metavar='N',
                    help="gpu id used for testing (default: '1')")

args = parser.parse_args()


# setting up the visible GPU
os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu_id

# loading network from path

if args.network_path in PRETRAINED:
    # pretrained networks (downloaded automatically)
    state = load_url(PRETRAINED[args.network_path], model_dir=os.path.join(get_data_root(), 'networks'))


# parsing net params from meta
# architecture, pooling, mean, std required
# the rest has default values, in case that is doesnt exist
net_params = {}
net_params['architecture'] = state['meta']['architecture']
net_params['pooling'] = state['meta']['pooling']
net_params['local_whitening'] = state['meta'].get('local_whitening', False)
net_params['regional'] = state['meta'].get('regional', False)
net_params['whitening'] = state['meta'].get('whitening', False)
net_params['mean'] = state['meta']['mean']
net_params['std'] = state['meta']['std']
net_params['pretrained'] = False

# load network
net = init_network(net_params)
net.load_state_dict(state['state_dict'])

# if whitening is precomputed
if 'Lw' in state['meta']:
    net.meta['Lw'] = state['meta']['Lw']

# setting up the multi-scale parameters
ms = list(eval(args.multiscale))
if len(ms)>1 and net.meta['pooling'] == 'gem' and not net.meta['regional'] and not net.meta['whitening']:
    msp = net.pool.p.item()
else:
    msp = 1

# moving network to gpu and eval mode
net.cuda()
net.eval()

# set up the transform
normalize = transforms.Normalize(
    mean=net.meta['mean'],
    std=net.meta['std']
)
transform = transforms.Compose([
    transforms.ToTensor(),
    normalize
])

# compute whitening

if len(ms)>1:
    Lw = net.meta['Lw'][args.whitening]['ms']
else:
    Lw = net.meta['Lw'][args.whitening]['ss']


# evaluate on test datasets
dataset = 'oxford5k'
    

    
def get_relevant(input_img):
    start = time.time()
    cfg = configdataset(dataset, os.path.join(get_data_root(), 'test'))
    images = [cfg['im_fname'](cfg,i) for i in range(cfg['n'])]
    qimages = [os.path.expanduser(input_img)]
    try:
        bbxs = [tuple(cfg['gnd'][i]['bbx']) for i in range(cfg['nq'])]
    except:
        bbxs = None  # for holidaysmanrot and copydays
    
    # vecs = extract_vectors(net, images, args.image_size, transform, ms=ms, msp=msp)
    vecs = torch.load('vecs.pth')
    qvecs = extract_vectors(net, qimages, args.image_size, transform, bbxs=bbxs, ms=ms, msp=msp)
    vecs = vecs.numpy()
    qvecs = qvecs.numpy()
    vecs_lw  = whitenapply(vecs, Lw['m'], Lw['P'])
    qvecs_lw = whitenapply(qvecs, Lw['m'], Lw['P'])
    scores = np.dot(vecs_lw.T, qvecs_lw)
    ranks = np.argsort(-scores, axis=0)
    top5 = {scores[top[0]][0]: images[top[0]] for i,top in enumerate(ranks[:5]) }
    print('>> {}: elapsed time: {}'.format(dataset, htime(time.time()-start)))
    return top5






print(get_relevant('data/test/oxford5k/jpg/all_souls_000202.jpg'))
